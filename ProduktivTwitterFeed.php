<?php 
    use TwitterAPIExchange;
    
    require_once('TwitterAPIExchange.php');

    class ProduktivTwitterFeed {
        public $twitterSettings;

        function __construct($twitterSettings) {
            $this->twitterSettings = $twitterSettings;
        }

        public function fetchTwitterUserFeed(){
            $twitterApi = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
            $getfield = '?screen_name=chamathme';
            $requestMethod = 'GET';

            $twitter = new TwitterAPIExchange($this->twitterSettings);
            return $twitter->setGetfield($getfield)->buildOauth($twitterApi, $requestMethod)->performRequest();
        }
    }

?>